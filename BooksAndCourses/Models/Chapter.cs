﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BooksAndCourses.Models
{
    public class Chapter
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChapterId { get; set; }

        [Required]
        [Display(Name = "Название")]
        [StringLength(255)]        
        public string Title { get; set; }

        [Display(Name = "Порядковый номер")]
        [StringLength(255)]
        public int Order { get; set; }

        [Display(Name = "Разделы")]
        public ICollection<Chapter> SubChapters { get; set; }

        [Display(Name = "Количество страниц")]
        public int? Pages { get; set; }

        [Display(Name = "Cтраница")]
        public int? FirstPage { get; set; }

        public int BookId { get; set; }
        public int? ParentChapterId { get; set; }
    }
}
