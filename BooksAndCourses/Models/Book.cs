﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BooksAndCourses.Models
{
    public class Book
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BookId { get; set; }

        [Required]
        [Display(Name = "Название книги")]
        [StringLength(255)]        
        public string Title { get; set; }

        [Display(Name = "Год")]
        [StringLength(255)]
        public int? Year { get; set; }

        [Display(Name = "Обложка")]
        [StringLength(255)]
        public string PathToCover { get; set; }

        [Display(Name = "Описание")]
        [StringLength(255)]
        public string PathToDescription { get; set; }

        [Display(Name = "Авторы")]
        public ICollection<Author> Authors { get; set; }

        [Display(Name = "Главы")]
        public ICollection<Chapter> Chapters { get; set; }

        [Display(Name = "Количество страниц")]
        public int? Pages { get; set; }

        [Display(Name = "Cтраница")]
        public int? FirstPage { get; set; }

        [Display(Name = "Издательство")]
        [StringLength(255)]
        public string Publishment { get; set; }

        [Display(Name = "Издание")]
        public int? Edition { get; set; }

        [NotMapped]
        [Display(Name = "Авторы")]
        public List<Author> AuthorsViewModel { get; set; }

        [NotMapped]
        [Display(Name = "Новый автор")]
        public string NewAuthor { get; set; }
    }
}
