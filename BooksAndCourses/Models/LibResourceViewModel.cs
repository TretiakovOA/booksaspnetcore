﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BooksAndCourses.Models
{
    public class LibResourceViewModel
    {
        [Display(Name = "Тип")]
        public LibResourceType Type { get; set; }
        public int Id { get; set; }

        [Display(Name = "Автор")]
        public string Author { get; set; }
        [Display(Name = "Наименование")]
        public string Title { get; set; }
    }

    public enum LibResourceType: byte
    {
        [Display(Name ="Книга")]
        Book = 1,
        [Display(Name = "Интернет-ресурс")]
        InternetResource
    }

    public static class AttributesHelperExtension
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            return enumValue.GetType().GetMember(enumValue.ToString())
                .First().GetCustomAttribute<DisplayAttribute>().Name;
        } 
    }
}
