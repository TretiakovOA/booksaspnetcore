﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BooksAndCourses.Models
{
    public class InternetResource
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InternetResourceId { get; set; }

        [Required]
        [Display(Name = "Имя ресурса")]
        [StringLength(255)]        
        public string Title { get; set; }

        [Display(Name = "Год")]
        [StringLength(255)]
        public int? Year { get; set; }

        [Display(Name = "Авторы")]
        public ICollection<Author> Authors { get; set; }

        [Display(Name = "Главная ссылка")]
        [Required]
        public string MainLink { get; set; }

        [NotMapped]
        [Display(Name = "Авторы")]
        public List<Author> AuthorsViewModel { get; set; }

        [NotMapped]
        [Display(Name = "Новый автор")]
        public string NewAuthor { get; set; }
    }
}
