﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BooksAndCourses.Models
{
    public class Author
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuthorId { get; set; }

        [Required]
        [Display(Name = "Имя автора")]
        [StringLength(255)]
        [Column("Author")]
        public string Title { get; set; }

        public int? BookId { get; set; }
        public int? InternetResourceId { get; set; }
    }
}
