﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksAndCourses.Models
{
    public class BooksContext: DbContext
    {
        public BooksContext(DbContextOptions<BooksContext> options) : base(options) { }

        public DbSet<Book> Books { get; set; }
        public DbSet<InternetResource> InternetResources { get; set; }
        public DbSet<Chapter> Chapters { get; set; }
        public DbSet<Author> Authors { get; set; }
    }
}
