﻿using System.Collections.Generic;
using System.Linq;

namespace BooksAndCourses.Models
{
    public static class DbInitializer
    {
        public static void Initialize(BooksContext context)
        {
            context.Database.EnsureCreated();

            if (context.Books.Any()) return;

            List<Book> books = InitBooks();
            List<InternetResource> internetResources = InitInternetResources();
            context.Books.AddRange(books);
            context.InternetResources.AddRange(internetResources);
            context.SaveChanges();

            List<Chapter> chapters = InitChapters(context);
            List<Author> authors = InitAuthors(context);
            context.Chapters.AddRange(chapters);
            context.Authors.AddRange(authors);
            context.SaveChanges();

            List<Chapter> subchapters = InitSubchapters(context);
            context.Chapters.AddRange(subchapters);
            context.SaveChanges();
        }

        private static List<Book> InitBooks()
        {
            List<Book> books = new List<Book>();

            Book book1 = new Book
            {
                Title = "Язык программирования C# 6.0 и платформа .NET 4.6",
                Edition = 7,
                Year = 2016,
                Pages = 1440,
                FirstPage = 1,
                Publishment = "Williams Publishing",
                PathToCover = "troelsen.jpg",
                PathToDescription = "troelsen.txt"
            };
            books.Add(book1);

            Book book2 = new Book
            {
                Title = "Illustrated C# 7: The C# Language Presented Clearly, Concisely, and Visually",
                Edition = 5,
                Year = 2018,
                Pages = 800,
                FirstPage = 1,
                Publishment = "Apress",
                PathToCover = "illustratedccharp.jpg",
                PathToDescription = "illustratedcsharp.txt"
            };
            books.Add(book2);

            Book book3 = new Book
            {
                Title = "Essential Angular for ASP.NET Core MVC",
                Edition = 1,
                Year = 2017,
                Pages = 298,
                FirstPage = 1,
                Publishment = "Apress",
                PathToCover = "essentialangular.jpg",
                PathToDescription = "essentialangular.txt"
            };
            books.Add(book3);

            return books;
        }

        private static List<InternetResource> InitInternetResources()
        {
            return new List<InternetResource>
        {
            new InternetResource
            {
                Title = "Полное руководство по языку программирования С# 7.0 и платформе .NET 4.7",
                MainLink = "https://metanit.com/sharp/tutorial/",
                Year = 2017
            },
            new InternetResource
            {
                Title = "Руководство по ASP.NET Core 2.0",
                MainLink = "https://metanit.com/sharp/aspnet5/",
                Year = 2017
            },
            new InternetResource
            {
                Title = "Руководство по Angular 6",
                MainLink = "https://metanit.com/web/angular2/",
                Year = 2018
            }
        };
        }

        public static List<Author> InitAuthors(BooksContext context)
        {
            Book book1 = context.Books.FirstOrDefault(b => b.Title.StartsWith("Язык программирования C# 6.0"));
            Book book2 = context.Books.FirstOrDefault(b => b.Title.StartsWith("Illustrated C# 7"));
            Book book3 = context.Books.FirstOrDefault(b => b.Title.StartsWith("Essential Angular"));
            InternetResource site1 = context.InternetResources.FirstOrDefault(s => s.Title.StartsWith("Полное руководство по языку программирования С#"));
            InternetResource site2 = context.InternetResources.FirstOrDefault(s => s.Title.StartsWith("Руководство по ASP.NET Core 2.0"));
            InternetResource site3 = context.InternetResources.FirstOrDefault(s => s.Title.StartsWith("Руководство по Angular 6"));
            List<Author> authors = new List<Author>
        {
            new Author {Title = "Троелсен Эндрю", BookId = book1.BookId},
            new Author {Title = "Джепикс Филипп", BookId = book1.BookId},
            new Author {Title = "Daniel Solis", BookId = book2.BookId},
            new Author {Title = "Cal Schrotenboer", BookId = book2.BookId},
            new Author {Title = "Adam Freeman", BookId = book3.BookId},
            new Author {Title = "Metanit", InternetResourceId = site1.InternetResourceId},
            new Author {Title = "Metanit", InternetResourceId = site2.InternetResourceId},
            new Author {Title = "Metanit", InternetResourceId = site3.InternetResourceId}
        };
            return authors;
        }

        private static List<Chapter> InitChapters(BooksContext context)
        {
            List<Chapter> chapters = new List<Chapter>();
            Book book2 = context.Books.FirstOrDefault(b => b.Title.StartsWith("Illustrated C# 7"));
            List<Chapter> chapters2 = new List<Chapter>
        {
            new Chapter {Order = 1, Title = "C# and the .NET Framework", FirstPage = 1, Pages = 16, BookId = book2.BookId },
            new Chapter {Order = 2, Title = "C# and .NET Core", FirstPage = 17, Pages = 6, BookId = book2.BookId},
            new Chapter {Order = 3, Title = "Overview of C# Programming", FirstPage = 23, Pages = 20, BookId = book2.BookId},
            new Chapter {Order = 4, Title = "Types, Storage and Variables", FirstPage = 43, Pages = 16, BookId = book2.BookId},
            new Chapter {Order = 5, Title = "Classes: The Basics", FirstPage = 59, Pages = 18, BookId = book2.BookId},
            new Chapter {Order = 6, Title = "Methods", FirstPage = 77, Pages = 52, BookId = book2.BookId},
            new Chapter {Order = 7, Title = "More About Classes", FirstPage = 129, Pages = 46, BookId = book2.BookId},
            new Chapter {Order = 8, Title = "Classes and Inheritance", FirstPage = 175, Pages = 42, BookId = book2.BookId},
            new Chapter {Order = 9, Title = "Expressions and Operators", FirstPage = 217, Pages = 44, BookId = book2.BookId},
            new Chapter {Order = 10, Title = "Statements", FirstPage = 261, Pages = 32, BookId = book2.BookId},
            new Chapter {Order = 11, Title = "Structs", FirstPage = 293, Pages = 10, BookId = book2.BookId},
            new Chapter {Order = 12, Title = "Enumerations", FirstPage = 303, Pages = 14, BookId = book2.BookId},
            new Chapter {Order = 13, Title = "Arrays", FirstPage = 317, Pages = 30, BookId = book2.BookId},
            new Chapter {Order = 14, Title = "Delegates", FirstPage = 347, Pages = 24, BookId = book2.BookId},
            new Chapter {Order = 15, Title = "Events", FirstPage = 371, Pages = 16, BookId = book2.BookId},
            new Chapter {Order = 16, Title = "Interfaces", FirstPage = 387, Pages = 28, BookId = book2.BookId},
            new Chapter {Order = 17, Title = "Conversions", FirstPage = 415, Pages = 30, BookId = book2.BookId},
            new Chapter {Order = 18, Title = "Generics", FirstPage = 445, Pages = 40, BookId = book2.BookId},
            new Chapter {Order = 19, Title = "Enumerators and Iterators", FirstPage = 485, Pages = 22, BookId = book2.BookId},
            new Chapter {Order = 20, Title = "Introduction to LINQ", FirstPage = 507, Pages = 58, BookId = book2.BookId},
            new Chapter {Order = 21, Title = "Introduction to Asynchronous Programming", FirstPage = 565, Pages = 64, BookId = book2.BookId},
            new Chapter {Order = 22, Title = "Namespaces and Assemblies", FirstPage = 629, Pages = 30, BookId = book2.BookId},
            new Chapter {Order = 23, Title = "Exceptions", FirstPage = 659, Pages = 22, BookId = book2.BookId},
            new Chapter {Order = 24, Title = "Preprocessor Directives", FirstPage = 681, Pages = 12, BookId = book2.BookId},
            new Chapter {Order = 25, Title = "Reflection and Attributes", FirstPage = 693, Pages = 28, BookId = book2.BookId},
            new Chapter {Order = 26, Title = "What's New in C# 6 and 7", FirstPage = 721, Pages = 30, BookId = book2.BookId},
            new Chapter {Order = 27, Title = "Other Topics", FirstPage = 751, Pages = 34, BookId = book2.BookId}
        };
            book2.Chapters = chapters2;
            chapters.AddRange(chapters2);

            Book book3 = context.Books.FirstOrDefault(b => b.Title.StartsWith("Essential Angular"));
            List<Chapter> chapters3 = new List<Chapter>
        {
            new Chapter {Order = 1, Title = "Understanding Angular and ASP.NET Core MVC", FirstPage = 1, Pages = 4, BookId = book3.BookId},
            new Chapter {Order = 2, Title = "Getting Ready", FirstPage = 5, Pages = 16, BookId = book3.BookId},
            new Chapter {Order = 3, Title = "Creating the Project", FirstPage = 21, Pages = 22, BookId = book3.BookId},
            new Chapter {Order = 4, Title = "Creating the Data Model", FirstPage = 43, Pages = 26, BookId = book3.BookId},
            new Chapter {Order = 5, Title = "Creating a Web Service", FirstPage = 69, Pages = 34, BookId = book3.BookId},
            new Chapter {Order = 6, Title = "Completing a Web Service", FirstPage = 103, Pages = 32, BookId = book3.BookId},
            new Chapter {Order = 7, Title = "Structuring an Angular Application", FirstPage = 135, Pages = 24, BookId = book3.BookId},
            new Chapter {Order = 8, Title = "Creating the Store", FirstPage = 159, Pages = 38, BookId = book3.BookId},
            new Chapter {Order = 9, Title = "Completing the Angular Store", FirstPage = 187, Pages = 40, BookId = book3.BookId},
            new Chapter {Order = 10, Title = "Creating Administration Features", FirstPage = 227, Pages = 24, BookId = book3.BookId},
            new Chapter {Order = 11, Title = "Securing the Application", FirstPage = 251, Pages = 28, BookId = book3.BookId},
            new Chapter {Order = 12, Title = "Preparing for Deployment", FirstPage = 279, Pages = 16, BookId = book3.BookId}
        };
            book3.Chapters = chapters3;
            chapters.AddRange(chapters3);
            return chapters;
        }

        private static List<Chapter> InitSubchapters(BooksContext context)
        {
            Book parentBook = context.Books.FirstOrDefault(b => b.Title.StartsWith("Illustrated C# 7"));
            Chapter parentChapter = context.Chapters.FirstOrDefault(c => c.BookId == parentBook.BookId && c.Order == 1);
            List<Chapter> subChapters = new List<Chapter>
        {
            new Chapter {Order = 1, Title = "Before .NET", FirstPage = 2, Pages = 1, BookId = parentBook.BookId, ParentChapterId = parentChapter.ChapterId},
            new Chapter {Order = 2, Title = "Enter Microsoft .NET", FirstPage = 3, Pages = 4, BookId = parentBook.BookId, ParentChapterId = parentChapter.ChapterId},
            new Chapter {Order = 3, Title = "Compiling to the Common Intermediate Language", FirstPage = 7, Pages = 1, BookId = parentBook.BookId, ParentChapterId = parentChapter.ChapterId},
            new Chapter {Order = 4, Title = "Compiling to Native Code and Execution", FirstPage = 8, Pages = 2, BookId = parentBook.BookId, ParentChapterId = parentChapter.ChapterId},
            new Chapter {Order = 5, Title = "The Common Language Runtime", FirstPage = 10, Pages = 1, BookId = parentBook.BookId, ParentChapterId = parentChapter.ChapterId},
            new Chapter {Order = 6, Title = "The Common Language Infrastructure", FirstPage = 11, Pages = 2, BookId = parentBook.BookId, ParentChapterId = parentChapter.ChapterId},
            new Chapter {Order = 7, Title = "Review of the Acronyms", FirstPage = 13, Pages = 1, BookId = parentBook.BookId, ParentChapterId = parentChapter.ChapterId},
            new Chapter {Order = 8, Title = "The Evolution of C#", FirstPage = 14, Pages = 1, BookId = parentBook.BookId, ParentChapterId = parentChapter.ChapterId},
            new Chapter {Order = 9, Title = "C# and the Evolution of Windows", FirstPage = 15, Pages = 1, BookId = parentBook.BookId, ParentChapterId = parentChapter.ChapterId}
        };
            parentChapter.SubChapters = subChapters;
            return subChapters;
        }
    }
}

