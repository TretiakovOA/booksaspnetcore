﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BooksAndCourses.Models;

namespace BooksAndCourses.Controllers
{
    public class HomeController : Controller
    {
        private readonly BooksContext _context;

        public HomeController(BooksContext ctx)
        {
            _context = ctx;
        }

        //GET: Home
        public IActionResult Index()
        {
            return View(GetResources());
        }

        //GET: Home/CreateBook
        public IActionResult CreateBook()
        {
            return View();
        }

        private List<LibResourceViewModel> GetResources()
        {
            List<LibResourceViewModel> resources = new List<LibResourceViewModel>();
            var books = _context.Books;
            var sites = _context.InternetResources;
            foreach (var b in books)
            {
                var model = CreateModel(book: b);
                resources.Add(model);
            }
            foreach (var s in sites)
            {
                var model = CreateModel(site: s);
                resources.Add(model);
            }
            return resources;
        }

        LibResourceViewModel CreateModel(Book book = null, InternetResource site = null)
        {
            if (book == null && site == null) return null;
            LibResourceViewModel model = new LibResourceViewModel();
            model.Type = book != null ? LibResourceType.Book : LibResourceType.InternetResource;
            model.Id = book?.BookId ?? site.InternetResourceId;

            var authors = book != null ? GetAuthorsOfBook(book) :
                _context.Authors.Where(a => a.InternetResourceId == site.InternetResourceId).ToList();
            if (authors.Count() >= 1) {
                var author = authors.FirstOrDefault().Title;
                if (authors.Count() > 1) author += " и др. ";
                model.Author = author;
            }
            model.Title = book?.Title ?? site.Title;
            return model;
        }

        private List<Author> GetAuthorsOfBook(Book book)
        {
            return _context.Authors.Where(a => a.BookId == book.BookId).ToList();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
